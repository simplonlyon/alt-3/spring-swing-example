package co.simplon.alt3.sprwing;

import java.awt.EventQueue;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import co.simplon.alt3.sprwing.ui.MainWindow;


@SpringBootApplication
public class SprwingApplication {

	public static void main(String[] args) {
		//On crée l'application Spring et on récupère son context qui nous permettra de
		//créer des instances des composants spring (controller/component/repository)
		ApplicationContext ctx = new SpringApplicationBuilder(SprwingApplication.class)
								.headless(false).run(args);

		//Pas sûr, mais je présume que ça va attendre que l'application Spring soit lancée
		//pour déclencher le contenu de ce truc
		EventQueue.invokeLater(() -> {
			//On récupère une instance de notre fenêtre via Spring et on la lance
			MainWindow window = ctx.getBean(MainWindow.class);
			window.run();
        });
	}

}
