package co.simplon.alt3.sprwing.ui;

import java.awt.Container;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * La classe Router va permettre de naviguer d'un JPanel à l'autre, elle contient
 * les constantes représentant les différentes pages, une référence à la MainWindow
 * et l'ApplicationContext de spring afin de pouvoir récupérer/créer les instances
 * de JPanel
 */
@Component
public class Router {
    public static final String LIST_DOG = "listDogPanel";
    public static final String ADD_DOG = "addDogPanel";

    @Autowired
    @Lazy
    private MainWindow mainWindow;

    @Autowired
    private ApplicationContext context;
    /**
     * Méthode permettant de changer le Panel principal de la  MainWindow
     * @param target La string lié au panel à afficher, utiliser une des constantes définies au dessus
     */
    public void navigate(String target) {
        //Selon la constante choisie
        switch (target) {
            case LIST_DOG:
            //On récupère l'instance associé dans le context et on l'envoie à la MainWindow
                mainWindow.changePanel(context.getBean(ListDogPanel.class));

                break;
            case ADD_DOG:
                mainWindow.changePanel(context.getBean(AddDogPanel.class));

                break;
        }
    }

    /**
     * Méthode qui renvoie le Panel par défaut à afficher
     */
    public Container defaultPanel() {
        return context.getBean(ListDogPanel.class);

    }

}
