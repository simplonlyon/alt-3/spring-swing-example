package co.simplon.alt3.sprwing.ui;

import java.awt.GridLayout;

import javax.annotation.PostConstruct;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class AddDogPanel extends JPanel {

    public AddDogPanel() {
        setLayout(new GridLayout(0,2));
        System.out.println("test");
    }

    @PostConstruct
    public void init() {
        add(new JLabel("Add Dog Panel"));
    }
    
}
