package co.simplon.alt3.sprwing.ui;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import co.simplon.alt3.sprwing.controller.DogController;
import co.simplon.alt3.sprwing.entity.Dog;


/**
 * On annote les panel swing en component afin que spring puisse les instancier et
 * donc pouvoir autowired nos controllers à l'intérieur
 * On rajoute également un @Lazy pour faire que le Panel ne soit instancié que 
 * lorsqu'on en a besoin et pas directement au lancement de l'application
 */
@Component
@Lazy
public class ListDogPanel extends JPanel {
    @Autowired
    private DogController controller;
    @Autowired
    private Router router;

    public ListDogPanel() {

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
    }

    @PostConstruct
    public void init() {

        
        List<Dog> liste = controller.allDogs();

        for (Dog dog : liste) {
            add(new JLabel("<html>Dog : "+dog.getName() +" breed : "+dog.getBreed()+"</html>"));
        }

        JButton navigateBtn = new JButton("Go to Add");
        navigateBtn.addActionListener((arg0) -> router.navigate(Router.ADD_DOG));
        add(navigateBtn);

    }

    
}
