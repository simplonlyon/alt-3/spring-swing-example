package co.simplon.alt3.sprwing.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//Si on veut pouvoir autowire des trucs dans un élément swing, il faudra que cet élément
//soit annoté en Component afin de pouvoir être lui même autowired par spring
@Component
public class MainWindow extends JFrame {

    @Autowired
    private Router router;


    public MainWindow() {
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void run() {

        setContentPane(router.defaultPanel());


        setVisible(true);
    }

    public void changePanel(JPanel panel) {
       
        setContentPane(panel);
        revalidate();
        repaint();
    }

    


}
