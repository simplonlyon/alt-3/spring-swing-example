package co.simplon.alt3.sprwing.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import co.simplon.alt3.sprwing.entity.Dog;
import co.simplon.alt3.sprwing.repository.DogRepository;

@Controller
public class DogController {
    @Autowired
    private DogRepository repo;
    
    public List<Dog> allDogs() {
        
        return repo.findAll();
    }
}
