package co.simplon.alt3.sprwing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.alt3.sprwing.entity.Dog;

@Repository
public interface DogRepository extends JpaRepository<Dog, Integer> {
    
}
