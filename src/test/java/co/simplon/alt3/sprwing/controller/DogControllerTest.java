package co.simplon.alt3.sprwing.controller;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import co.simplon.alt3.sprwing.entity.Dog;
import co.simplon.alt3.sprwing.repository.DogRepository;

@ExtendWith(MockitoExtension.class)
public class DogControllerTest {
    @InjectMocks
    DogController controller;

    @Mock
    DogRepository repo;


    @Test
    void testAllDogs() {
        Dog dog = new Dog(1, "nametest", "breedtest", 1);
        //Indiquer ce que renvoie une méthode du mock le temps de ce test
        Mockito.when(repo.findAll()).thenReturn(List.of(dog));

        assertTrue(controller.allDogs().contains(dog));
        //Vérifier que telle ou telle méthode du mock a été appelée
        verify(repo).findAll();
        
    }

}
